ROOT_DIR     = File.expand_path('../..', __FILE__)
STATIC_FILES = {
  '/'         => File.read('assets/empty.html'),
  '/frest.js' => File.read('assets/frest.js')
}
App          = ->(env) {
  req  = Rack::Request.new(env)
  path = req.path_info

  static = STATIC_FILES[path]
  if static
    ['200', { 'Content-Type' => 'text/html' }, [static]] if static
  else
    [200, {}, ['Here']]
  end
}

require 'faye'
require 'permessage_deflate'
require File.expand_path('../app', __FILE__)

Faye::WebSocket.load_adapter('thin')

use Faye::RackAdapter, mount: '/ws', timeout: 25 do |bayeux|
  $bayeux = bayeux
  $bayeux.add_websocket_extension(PermessageDeflate)
end

run App

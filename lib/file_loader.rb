using StringCall

require 'listen'
require 'yaml'

module FREST
  module FileLoader
    CONSTANT_SOURCE_PATH = File.expand_path(__dir__ + '/../source')
    DEFAULT_SOURCE_PATH  = File.expand_path(__dir__ + '/../source_weak')
    ASSET_PATH           = File.expand_path(__dir__ + '/../assets')
    DEFAULT_PATHS        = [DEFAULT_SOURCE_PATH, DEFAULT_SOURCE_PATH, ASSET_PATH]
    ROOT_FILE_NAME       = 'self'

    module_function

    def load(
      path:,
      mode: :default,
      source_path: nil,
      **_
    )
      return ROOT_FILE_NAME.load if path.empty?

      path = canonicalize_path([*path])

      unless source_path
        source_path = (mode == :default) ? CONSTANT_SOURCE_PATH : DEFAULT_SOURCE_PATH
      end

      result = load_from_filesystem(
        path:        path,
        source_path: source_path
      )

      return result if result

      nil
    end

    def call(*b, **c)
      f = load(*b, **c)
      f.(*b, **c) if f
    end

    def metadata(*b, **c)
      f = load(*b, **c)
      f.metadata(*b, **c) if f
    end


    # private — can't call canonicalize_path from above when private?

    def canonicalize_path(path)
      if path.first == ''
        path[1..-1]
      else
        path
      end
    end

    def maybe_initialize(
      path:,
      source_path:
    )
      if File.exist?(f = File.join(source_path, path, 'init.rb'))
        require_path f
      end
    end

    def load_from_filesystem(
      path:,
      source_path:,
      **_
    )

      thread          = Thread.current
      thread[:new_fn] = nil

      maybe_load_ruby_file(
        source_path: source_path,
        path:        path,
        thread:      thread
      ) ||
        maybe_load_other_file(
          source_path: source_path,
          path:        path
        ) ||
        nil
    end

    def maybe_load_ruby_file(
      source_path:,
      thread:,
      path:
    )
      pth = File.join(source_path, path)

      return false unless ruby_file? pth

      begin
        loaded = require_path pth
      rescue Exception => e
        nil
      end

      if loaded
        return thread[:new_fn]
      else
        false
      end
    end

    def maybe_load_other_file(
      source_path:,
      path:
    )
      p = File.join source_path, path

      case
      when File.directory?(p)
        d = 'directory_proxy'.load

        return FREST.defn do |
          *b,
          **c|

          d.(*b, **(c.merge path: p))
        end

      when File.exist?(p)
        if %w{yml yaml json}.any? File.extname(p)
          return YAML.load p
        else
          return File.read p
        end
      else
        nil
      end
    end


    def require_path(
      path
    )
      f = final_path(path)
      require_relative f if File.exist?(f) rescue nil
    end

    def ruby_file? path
      return false if path.empty?

      ext = File.extname(path)
      ext == '.rb' || ext == ''
    end

    def exists_file? path
      File.exist? File.join(path)
    end

    def final_path(path)
      if File.directory? path
        p = path + '/self.rb'
        if File.exist? p
          return p
        else
          return DIRECTORY_PROXY
        end
      else
        path += '.rb' if File.extname(path) == ''
      end

      path
    end

    Thread.new do
      Listen.to DEFAULT_SOURCE_PATH, DEFAULT_SOURCE_PATH do |modified, added, removed|
        [modified, added].each do |pth|
          if pth
            p "Reloading #{pth}"
            load_from_filesystem path: canonicalize_path(path)
          end
        end
      end
    end
  end
end

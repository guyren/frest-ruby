using StringCall

require 'file_loader'

module FREST
  module_function

  def defn **args, &block
    Thread.current[:new_fn] = RubyFn.new **args, &block
  end
end

class RubyFn
  def initialize **metadata, &block
    @metadata = metadata
    @fn   = block
  end

  def matches?(
    **matches
  )
    if m = @metadata[:match]
      begin
        return m.call(**matches)
      rescue
        nil
      end
    else
      true
    end
  end

  def call *path, **args
    return @fn.(
      *path,
      context: self,
      **args
    ), @metadata # NB
  end

  def metadata *path, **args
    @metadata
  end
end

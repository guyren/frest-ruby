require 'json'

module StringCall
  refine String do
    UUID_PATTERN = /^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/

    def init
      return if @inited

      begin
        @parsed        = JSON.parse self
        @callable_hash = @parsed.is_a? Hash
      rescue JSON::ParserError
      end

      @inited = true
    end

    def call(context: @default_context, **args)
      init

      if @callable_hash
        fn = @parsed.keys.first
        context.(path: fn, **@parsed[fn], **args)
      else
        if context
          context.(path: self, **args)
        else
          FREST::FileLoader.call(**(args.merge(path: self)))
        end
      end
    end

    def load(**args)
      if @callable_hash
        fn = @parsed.keys.first
        FREST::FileLoader.load(**(args.merge(path: fn, **@parsed[fn])))
      else
        FREST::FileLoader.load(**(args.merge(path: self)))
      end
    end
  end
end

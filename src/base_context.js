export function BaseContext() {
  _context_strong:  (key) => {raise 'Implement _context_strong'}
  _context_weak:    (key) => {raise 'Implement _context_weak'}
}

BaseContext.prototype.value = function value(key) {
  return this._content_strong[key] || this._content_weak[key]
}

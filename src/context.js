import BaseContext from './base_context'

export function Context(content) {
  this._content_strong  = {}
  Object.assign(content.strong, this._content_strong)
  this._content_weak    = {}
  Object.assign(content.weak, this._content_weak)

  BaseContext.call(this)
}

Context.prototype = Object.create(BaseContext.prototype)

Context.prototype._valueStrong = function _valueStrong(key) {
  return this._content_strong[key]
}

Context.prototype._valueWeak = function _valueWeak(key) {
  return this._content_weak[key]
}

Context.prototype.chainWithSub = function chainWithSub(sub) {
  return new ContextChain(sub, this)
}

Context.prototype.chainWithSub = function chainWithSub(sub, sup) {
  return new ContextChain(sub, sup)
}


// ContextChain ---------------------

function ContextChain(sub, sup) {
  this.sub = sub
  this.sup = sup
}

ContextChain.prototype.value = function value(key) {
  return sup._valueStrong(key) ||
    sub.value(key) ||
    sup._valueWeak(key)
}

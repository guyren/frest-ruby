import UUID          from 'uuid'
import Context       from './context'
import RemoteContext from './remote_context'

(() => {
  document.addEventListener('contextmenu', (evt) => {
    const target = evt.target
    const id = target.id

    evt.cancelBubble = true
    evt.preventDefault()
  })

  window.fullContext = new WSContext
})()
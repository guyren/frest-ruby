(() => {
  var client = document.createElement('script')
  client.src = '/ws/client.js'
  client.onreadystatechange = () => {
    var triggers = {},
        cache    = {},
        fns      = {}
  }
    FREST.fayeClient   = new Faye.Client('/ws')
    FREST.channel      = new RemoteContext

    document.head.appendChild(client)
  })()
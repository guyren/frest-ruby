require 'rack'
require_relative 'lib/file_loader'

STATIC_FILES = {
  '/'          => File.read('assets/empty.html'),
  '/frest.js' => File.read('assets/frest.js')
}

app = Proc.new do |env|
  req  = Rack::Request.new(env)
  path = req.path_info

  if static = STATIC_FILES[path]
    ['200', { 'Content-Type' => 'text/html' }, [static]] if static
  else
    [200, {}, serve_function(
      path: path,
      env: env
    )]
  end
end

Rack::Handler::Thin.run app

def serve_function(
  path:,
  env:
)
  if env.method == :options
    fn = FREST::FileLoader.meta
  else

    fn = FREST::FileLoader.call(
      path: path
    )
  end
end
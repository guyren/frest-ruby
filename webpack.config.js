const path = require('path');

module.exports = {
  mode:  'development',
  entry: './src/frest.js',
  output: {
    filename: 'frest.js',
    path: path.resolve(__dirname, 'assets')
  }
};
